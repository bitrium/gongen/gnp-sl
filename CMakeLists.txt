cmake_minimum_required(VERSION 3.18)

project(GNPServerList C CXX)
set(CMAKE_CXX_STANDARD 20)
add_compile_definitions(_CRT_SECURE_NO_WARNINGS)

if (NOT DEFINED GONGEN_DIR)
	message(FATAL_ERROR "Please define GONGEN_DIR!")
endif ()
set(gongen_directory ${GONGEN_DIR})
include("${GONGEN_DIR}/cmake/gongen.cmake")
gongen_config(MODULES common net)

file(GLOB_RECURSE sources "src/*.cpp")
add_executable(gnp_sl ${sources})
target_link_libraries(gnp_sl PUBLIC gongen)
target_include_directories(gnp_sl PUBLIC "src")
#file(CREATE_LINK "${CMAKE_CURRENT_SOURCE_DIR}/assets" "${CMAKE_CURRENT_BINARY_DIR}/assets" SYMBOLIC)
